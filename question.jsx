import React from "react";
import ReactDOM from "react-dom";

import "./styles.css";

// if name = ? then not show,
// if no value then show button,
// else show name: value.
const testLIst = [
  {
    name: "m1",
    value: 1
  },
  {
    name: "?",
    value: 2
  },
  { name: "m3" }
];

function isQ(str) {
  if (str === "?") {
    return true;
  } else {
    return false;
  }
}

function App() {
  return (

  );
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
