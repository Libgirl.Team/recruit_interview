This is the repository for interview questions and template.

**Links**  
[new sandbox](https://codesandbox.io/s/new)  
[ts question](https://codesandbox.io/s/ts-question-qlfts)  
[js question](https://codesandbox.io/s/js-question-dcjyz)  
[js answer](https://codesandbox.io/s/recruit-answer-js-duzk1)  
[ts answer](https://codesandbox.io/s/recruit-answer-ts-n3cy1)  
[word search](https://playcode.io/472131?tabs=script.ts,preview,console)