import React from "react";
import ReactDOM from "react-dom";

import "./styles.css";

interface Item {
    name: string;
    value?: number;
}

// if name = ? then not show,
// if no value then show button,
// else show name: value.
const testLIst = [
    {
        name: "m1",
        value: 1
    },
    {
        name: "?",
        value: 2
    },
    { name: "m3" }
];

function isQ(str: string) {
    if (str === "?") {
        return true;
    } else {
        return false;
    }
}

function fc(list: Item[]): Array<JSX.Element> {
    return list.map(item => {
        if (isQ(item.name)) {
            return null;
        } else {
            if (item.value) {
                return <p>{`${item.name}: ${item.value}`}</p>;
            } else {
                return (
                    <div>
                        <p style={{ display: "inline" }}>{`${item.name}: `}</p>{" "}
                        <button style={{ display: "inline" }}>add</button>
                    </div>
                );
            }
        }
    });
}

function App() {
    return <div>{fc(testLIst)}</div>;
}

const rootElement = document.getElementById("root");
ReactDOM.render(<App />, rootElement);
